Django Boilerplate
==================

Instalação
----------

Faça um clone da aplicação:
    
    $ git clone git@bitbucket.org:carlosrberto/django-boilerplate.git
    $ cd /path/to/project

Criar um ambiente para a apliacação:

    $ virtualenv env  --no-site-packages
    
Inicie o ambiente:

    $ source path/to/env/bin/activate

Ou se estiver usando virtualenvwrapper:

    $ mkvirtualenv env
    
Instalar as dependencias da aplicação:

    $(env) pip install -r requeriments.txt

Criar um arquivo de configuração local usando o template em app/settings/development.txt, ajustar as configurações necessárias:

    $(env) cp app/settings/development.txt app/settings/development.py

Iniciar a aplicação usando servidor do django

    $(env) fab rund

ou:

    $(env) python app/manage.py ruserver 0.0.0.0:8000


Ao instalar novas dependencias atulizar o requeriments.txt

    $(env) fab req_update