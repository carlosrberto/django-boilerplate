# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import Context, loader, RequestContext
from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import redirect
from django.utils import simplejson
from django.db.models import Q
from util.util import pretty_date
from util import util as U
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from itertools import chain
from sorl.thumbnail.main import DjangoThumbnail
from facebook.api import pagePosts as facebookPagePosts
from twitter.api import userTweets
from conteudo.models import Post

import re
import time
import random

def index(request):
	VARS = {}
	return render_to_response('index.html', VARS, context_instance=RequestContext(request))

def tags(request, slug=None):
	if slug:
		pass
	else:
		return redirect('index')
	VARS = {
		'slug' : slug,
	}
	return render_to_response('tag.html', VARS, context_instance=RequestContext(request))

def search(request, slug=None):
	if slug:
		pass
	else:
		return redirect('index')
	VARS = {
		'slug' : slug,
	}
	return render_to_response('search.html', VARS, context_instance=RequestContext(request))
	
def post_detalhe(request, slug=None):
	posts = Post.objects
	post = posts.filter(slug=slug)
	
	if	post:
		post = post[0]
		tags = [tag.pk for tag in post.tags.all()]
		relacionados = posts.exclude(id=post.id)
		portfolios = relacionados.filter(tipo='Job', tags__in=tags).distinct()[:10]
		clientes = relacionados.filter(tipo='Cliente', tags__in=tags).distinct()[:10]
		profissionais = relacionados.filter(tipo='Profissional', tags__in=tags).distinct()[:10]
	
	VARS = {
		'post' : post,
		'portfolios' : portfolios,
		'clientes' : clientes,
		'profissionais' : profissionais
	}
	return render_to_response('post.html', VARS, context_instance=RequestContext(request))
	
	
def posts(request, slug=None):
	
	# página de detalhe do post
	if	slug:
		return post_detalhe(request, slug)
	
	# lista de posts
	max_posts = 30
	postsList = Post.objects
	
	if request.GET and request.GET.get('page', False):
		
		pageNumber = request.GET.get('page', False)
		query = request.GET.get('query', False)
		tag = request.GET.get('tag', False)
		
		if	query:
			termo = query
			postsList = postsList.filter(
	            Q(titulo__icontains=termo) |
	            Q(texto__icontains=termo) |
	            Q(tags__tag__icontains=termo) 
	        )
			
		if	tag:
			#filtros pass tag
			postsList = postsList.filter(tags__in=[tag])
		
		if	query or tag:
			postsList = postsList.distinct()
		else:
			postsList = postsList.all()
			
		postsList = postsList[:max_posts]
		
		if	tag:
			result_list = list( chain( postsList ) )
		else:
			result_list = list( chain( postsList, facebookPagePosts( query ), userTweets( query ) ) )
		
	postsListParcial = []
	data = []
	
	if	result_list:
		for	post in result_list:
			# models
			if	hasattr(post, '_meta'):
				tags = []
				for tag in post.tags.all():
					tags.append({'name':tag.tag, 'id':tag.id})
				if	post.get_foto():
					image = DjangoThumbnail(post.get_foto().foto, U.GRID_IMAGE_SIZE[str(post.tamanho)])
					imageData = { 'url': image.absolute_url, 'width' : image.width(), 'height' : image.height() }
				else:
					imageData = None
				
				unixtime = int(time.mktime(post.data.timetuple())*1000)
				
				data.append({
				 	'unixtime' : unixtime,
					'id' : post.pk,
					'slug' : post.slug,
					'tipo' : post.tipo,
					'titulo' : post.titulo,
					'subtitulo' : post.subtitulo,
					'css_size_class' : post.css_size_class(),
					'css_border_class' : post.css_border_class(),
					'css_background_class' : post.css_background_class(),
					'css_color_class' : post.css_color_class(),
					'tags' : tags,
					'image' : imageData,
					'origin' : 'site',
				})
			
			else:
				# facebook posts
				if	post.get('message', False):
					message = post['message']
					regex = re.search("(?P<url>http?://[^\s]+)", message)
					if	regex:
						link = regex.group("url")
						html_link = '<a href="%s" target="_blank">%s</a>' % (link, link)
						message = message.replace(link, html_link)
					data.append({
						'unixtime' : post['created_time'],
						'link' : post['permalink'],
						'message' : message,
						'created_time' : pretty_date(post['created_time']),
						'css_size_class' : 'size1',
						'css_border_class' : 'border-feed',
						'origin' : 'facebook',
					})
					
				else:
					created_time = post['created_at']
					ts = time.strptime(created_time,'%a %b %d %H:%M:%S +0000 %Y')
					unixtime = int(time.mktime(ts))
					created_time = pretty_date(unixtime)
					# twitter posts
					message = post['text']
					regex = re.search("(?P<url>http?://[^\s]+)", message)
					if	regex:
						link = regex.group("url")
						html_link = '<a href="%s" target="_blank">%s</a>' % (link, link)
						message = message.replace(link, html_link)
					data.append({
						'unixtime' : unixtime,
						'link' : 'https://twitter.com/%s/status/%s' % (settings.TWITTER_USERNAME, post['id']),
						'message' : message,
						'created_time' : created_time,
						'css_size_class' : 'size1',
						'css_border_class' : 'border-feed',
						'origin' : 'twitter',
					})
		
		# print data
		data = sorted( data, key=lambda k: -k['unixtime']) 
		
		paginator = Paginator(data, 20)
		
		try:
			postsListParcial = paginator.page(pageNumber).object_list
		except:
			postsListParcial = []
			
	return HttpResponse(simplejson.dumps(postsListParcial), mimetype='application/json')
	
