# -*- coding: utf-8 -*-
import os, sys

SITE_NAME = 'Aloha Beach Play'
SETTINGS_PATH = os.path.abspath(os.path.dirname(__file__))
PROJECT_PATH = os.path.join(SETTINGS_PATH, '../')
OS_SYSTEM = os.name

ADMINS = (
	('pubdesign', 'pubservidores@pubdesign.com.br'),
)

MANAGERS = ADMINS

TIME_ZONE = 'America/Sao_Paulo'

LANGUAGE_CODE = 'pt-Br'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

ROOT_URLCONF = 'urls'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')
STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(PROJECT_PATH, '../static')

TEMPLATE_DIRS = (
	os.path.join(PROJECT_PATH, '../static/site/templates'),
	os.path.join(PROJECT_PATH, '../static/admin/templates'),
	os.path.join(PROJECT_PATH, 'templates')
)

STATICFILES_DIRS = (
	os.path.join( PROJECT_PATH, '../static' ),
)
STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = '%2fflrovd089rl*x08av#93@j(&fox_+34iy)d^mp^1m4w=rvo'


TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
)


MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
)


TEMPLATE_CONTEXT_PROCESSORS = (
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.request',
	'django.core.context_processors.static',
)


INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.localflavor',
	'django.contrib.messages',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.staticfiles',
	# Apps

	# Main App
)

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'handlers': {
		'mail_admins': {
			'level': 'ERROR',
			'class': 'django.utils.log.AdminEmailHandler'
		}
	},
	'loggers': {
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': True,
		},
	}
}