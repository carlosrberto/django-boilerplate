# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

# -------------------------------------------------------------------
# servindo arquivos estaticos no desenvolvimento
# -------------------------------------------------------------------
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns('',
	#ADMIN
	# url(r'^admin/', include(admin.site.urls)),
	
	#INDEX
	url(r'^$', 'app_views.views.home', name='home'),
)


# urls estaticas
urlpatterns += staticfiles_urlpatterns()

# urls para runserver
import sys, os
if 'runserver' in sys.argv or 'runserver_plus':
	# POSIX
	if settings.OS_SYSTEM=='posix':
		urlpatterns += patterns( 'django.views',
			( r'^media/(?P<path>.*)$', 'static.serve', { 'document_root':settings.MEDIA_ROOT } ),
		)
	# NAO-POSIX
	if not settings.OS_SYSTEM=='posix':
		urlpatterns += patterns( 'django.views',
			( r'^media/(?P<path>.*)$', 'static.serve', { 'document_root':settings.MEDIA_ROOT } ),
			( r'^static/(?P<path>.*)$', 'static.serve', { 'document_root':os.path.join(settings.PROJECT_PATH, '../static') } ),
		)
	# ERROS
	urlpatterns += patterns( 'django.views',
		(r'^404/$', 'defaults.page_not_found'),
		(r'^500/$', 'defaults.server_error'),
	)