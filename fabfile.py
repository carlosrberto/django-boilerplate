# -*- coding: utf-8 -*-
from fabric.api import *

prod_server = 'user@server.com:22'

def prod():
	"""configura o ambiente de producao"""
	env.hosts = [prod_server]
	env['dir'] = '~user/django/projects'
	
def pull():
	"""executa o comando git pull no ambiente configurado"""
	with cd(env['dir']):
		run('git pull') 

def revert():
	""" Revert git via reset --hard @{1} """
	with cd(env['dir']):
		run('git reset --hard @{1}')

def req_update():
	"""atualiza o requeriments.txt com as dependências da aplicação"""
	local('pip freeze > requeriments.txt')
	
def req_install():
	"""instala as dependências para a aplicação"""
	local('pip install -r requeriments.txt')
	
def rund(port=8000, params=''):
	"""inicia a aplicação com o servidor do django"""
	local('python app/manage.py runserver 0.0.0.0:%s %s' % (port, params))
	
def rung(port=8000, params='-w 4'):
	"""inicia a aplicação usando gunicorn"""
	local('gunicorn_django -b 0.0.0.0:%s %s' % (port, params))
	
def killg():
	"""mata os processo do gunicorn"""
	local('ps -ef | grep gunicorn_django | grep -v grep | awk \'{print $2}\' | xargs kill -9')